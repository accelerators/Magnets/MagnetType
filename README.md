# MagnetType

Tango class to ease control of all EBS main magnets of one specific type
(all quadrupoles, all sextupoles or all octupoles)

## Cloning

To clone the project, type

```
git clone git@gitlab.esrf.fr:accelerators/Magnets/MagnetType.git
```

## Documentation

Pogo generated doc on commands, attributes,... in **doc_html** folder

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies **(Delete If Does Not Apply)**

* C++11 compliant compiler.

### Build

Instructions on building the project.

Make example:

```bash
cd MagnetType
make
```
